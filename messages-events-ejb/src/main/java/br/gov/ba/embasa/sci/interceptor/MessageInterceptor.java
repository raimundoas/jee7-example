package br.gov.ba.embasa.sci.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@EventoSCI
@Interceptor
public class MessageInterceptor {

	public MessageInterceptor() {
		// TODO Auto-generated constructor stub
	}

	@AroundInvoke
	public Object aroundInvoke(InvocationContext ic) throws Exception {
		
		Object result = ic.proceed();
		
		for(Object o : ic.getParameters())
			System.out.println("message: " + o.toString());
		
		return result;
	}

}
